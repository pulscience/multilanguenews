# -*- coding: utf-8 -*-
#from __future__ import unicode_literals


from django.views.generic import TemplateView
from django.http import HttpResponse
from django.shortcuts import render
from django.template import loader, Context
from recherche.forms import RechercheForm
import urllib.request, lxml
import re
import sys
from urllib.request import urlopen
from bs4 import BeautifulSoup
import subprocess
from tt import *
from articles import *
# Create your views here.

class RechercheView(TemplateView):
	template_name = 'recherche/index.html'

	def get(self, request):	
		form = RechercheForm()
		return render(request, self.template_name, {'form':form})

	def post(self, request):
		print("Request received")
		form = RechercheForm(request.POST)
		if form.is_valid():
			query = form.cleaned_data['query']
			language = form.cleaned_data['language']
			param={'langue' : language, 'requete' : query}
			
			liste_mots=traduction(param)

			le_monde="http://www.lemonde.fr/international/rss_full.xml"
			ny_times="http://rss.nytimes.com/services/xml/rss/nyt/World.xml"
			bbc="http://feeds.bbci.co.uk/news/world/rss.xml"
			el_pais="http://ep00.epimg.net/rss/internacional/portada.xml"

			
			matched_in_le_monde=[]
			matched_in_bbc =[]
			matched_in_ny_times=[]
			matched_in_el_pais=[]

			if language == 'fr':
				
				matched_in_le_monde+= correspondance_articles(liste_mots['requete'],liste_mots['synonymes'], le_monde)
				matched_in_bbc+= correspondance_articles(liste_mots['trad_requete_anglais'],liste_mots['trad_syn_anglais'], bbc)
				matched_in_el_pais+= correspondance_articles(liste_mots['trad_requete_espagnol'],liste_mots['trad_syn_espagnol'], el_pais)

			elif language == 'en':

				matched_in_le_monde+= correspondance_articles(liste_mots['trad_requete_français'],liste_mots['trad_syn_français'], le_monde)
				matched_in_bbc+= correspondance_articles(liste_mots['requete'],liste_mots['synonymes'], bbc)
				matched_in_el_pais+= correspondance_articles(liste_mots['trad_requete_espagnol'],liste_mots['trad_syn_espagnol'], el_pais)

			elif language == 'es':
				print("Request is in spanish")
				matched_in_le_monde+= correspondance_articles(liste_mots['trad_requete_français'],liste_mots['trad_syn_français'], le_monde)
				matched_in_bbc+= correspondance_articles(liste_mots['trad_requete_anglais'],liste_mots['trad_syn_anglais'], bbc)
				matched_in_el_pais+= correspondance_articles(liste_mots['requete'],liste_mots['synonymes'], el_pais)
			

			args = {'form': form, 'query': query, 'language': language, 'items_le_monde': matched_in_le_monde, 'items_bbc' : matched_in_bbc, 'items_el_pais': matched_in_el_pais}
			#subprocess.call("/home/rania/Bureau/multilanguenews/recherche/tst.py")
			#sys.argv=["cc","gg"]
			#exec(open("/home/rania/Bureau/multilanguenews/recherche/tst.py").read())
			
			return render(request, self.template_name, args)

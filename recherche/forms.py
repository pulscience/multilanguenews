from django import forms
from recherche.choices import * 
class RechercheForm(forms.Form):
	language = forms.ChoiceField(choices = LANGUAGE_CHOICES, label="", initial='', widget=forms.Select(attrs={'class':'form-control'}), required=True)
	query = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control form-control-lg'}))
	
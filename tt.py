import nltk
from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.tokenize import WordPunctTokenizer
from nltk.corpus import wordnet
from googletrans import Translator
import urllib.request, lxml
from urllib.request import urlopen
from bs4 import BeautifulSoup
translator = Translator(service_urls=['translate.google.com','translate.google.co.kr',])
#from nltk.translate import AligneSent , Alignement
from stanfordcorenlp import StanfordCoreNLP

import re
def traduction(param):
    query=param['requete']
    language=param['langue']
    if language == 'fr':
        liste=trad_fr(query)
    else:
        if language == 'en':
            liste=trad_en(query)
        else:
            liste=trad_es(query)
    return liste
def trad_en(query):
    synonyms = []
    i = 0
    z = 1
    c = 0
    cpt = 0

    tablequery = []
    tablequery_fr = []
    tablequery_es = []

    tablesyn_fr = []
    tablesyn_es = []
    tablesyn_en = []
    table_nom_propre = []
    table_nom_propre_fr = []
    table_nom_propre_es = []
    np = 0
    tabletokenize = []

    tabletagged =[]
    #query = input("rechercher :" )
    #créer une liste de mot , ponctuation ....
    tabletokenize = WordPunctTokenizer().tokenize(query)
    # tagger les entité
    #nlpen = StanfordCoreNLP(r'/home/rania/stanford-corenlp-full-2017-06-09', lang='en')
    nlpen = StanfordCoreNLP(r'/Library/Java/JavaVirtualMachines/jdk-9.0.1.jdk/Contents/Home/lib/stanford-corenlp-full-2017-06-09', lang='en')
    #tabletagged = nltk.pos_tag(tabletokenize)
    tabletagged = nlpen.pos_tag(query)
    for word in tabletagged:
        stringword = str(word)
        r1 = re.search(r"(NNS|NN')" , stringword)
        r2 = re.search(r"VB(.)?" , stringword)
        r3 = re.search(r"(NNP|NNPS)" , stringword)
        r4 = re.search(r"(ADJ)" , stringword)
        if (r1 or r2  or r4 or r3) is not None:
            tablequery.append(tabletokenize[i])
            for syn in wordnet.synsets(tabletokenize[i]):
                for l in syn.lemmas():
                    synonyms.append(l.name())
            while cpt < 2 and len(synonyms) > c :
                #print(str(len(synonyms))+"  "+str(c)+"\n")
                if synonyms[c]  in tablesyn_en or (synonyms[c] == tabletokenize[i])  :
                    c = c + 1
                else :
                    # st =  synonyms[c]
                    # r= re.search(r"(\_)" ,st)
                    # print(r)
                    # if r is not None:
                    #     st = st.replace("_", " ")
                    #     synonyms[c] = st
                    cpt = cpt+1
                    tablesyn_en.append(synonyms[c])
                    c = c + 1
            synonyms.clear()
            z = z + cpt
            i = i + 1
            c = 0
        else :
            i = i + 1

    translations = translator.translate(tablesyn_en, dest='fr')
    for translation in translations:
        tablesyn_fr.append(translation.text)

    translations = translator.translate(tablesyn_en, dest='es')
    for translation in translations:
        tablesyn_es.append(translation.text)

    translations = translator.translate(tablequery, dest='fr', src='en')
    for translation in translations:

        tablequery_fr.append(translation.text)

    translations = translator.translate(tablequery, dest='es', src='en')
    for translation in translations:
        tablequery_es.append(translation.text)

    for i in tablequery:
        if i not in tablequery_fr:
            tablequery_fr.append(i)
        if i not in tablequery_es:
            tablequery_es.append(i)
        #tablequery_fr.append(table_nom_propre_fr)
        #tablequery_es.append(table_nom_propre_es)

  #  print(tablequery)
   # print(tablesyn_en)
    #print(tablesyn_fr)
    #print(tablesyn_es)

    l={}
    l['requete']=tablequery
    l['synonymes']=tablesyn_en
    l['trad_syn_français']=tablesyn_fr
    l['trad_syn_espagnol']=tablesyn_es
    l['trad_requete_français']=tablequery_fr
    l['trad_requete_espagnol']=tablequery_es
    # l.append(tablequery)
    # l.append(tablesyn_en)
    # l.append(tablesyn_fr)
    # l.append(tablesyn_es)
    # l.append(tablequery_fr)
    # l.append(tablequery_es)
    return l
def trad_fr(query):
    table_nom_propre = []
    table_nom_propre_en = []
    table_nom_propre_es = []
    np = 0
    #nlpfr = StanfordCoreNLP(r'/home/rania/stanford-corenlp-full-2017-06-09', lang='fr')
    nlpfr = StanfordCoreNLP(r'/Library/Java/JavaVirtualMachines/jdk-9.0.1.jdk/Contents/Home/lib/stanford-corenlp-full-2017-06-09', lang='fr')
    synonyms = []
    i = 0
    z = 1
    c = 0
    cpt = 0
    tablesyn_en = []
    tablequery = []
    tablesyn_fr = []
    tablesyn_es = []


    tablequery_en = []
    tablequery_es = []

    tabletokenize = []
    tabletagged =[]
    #query = input("rechercher :" )
    #créer une liste de mot , ponctuation ....
    tabletokenize = WordPunctTokenizer().tokenize(query)
    # tagger les entité
    tabletagged = nlpfr.pos_tag(query)
    for word in tabletagged:
        stringword = str(word)
        r1 = re.search(r"(NC)" , stringword)
        r2 = re.search(r"V(.)?" , stringword)
        r3 = re.search(r"ADJ" , stringword)
        r4 = re.search(r"(NPP|[A-Z])" , stringword)
        # r3 = re.search(r"'(n(.)*?)'" , stringword)
        # r4 = re.search(r"'(v(.)*?)'" , stringword)
        if (r1 or r2 or r3 or r4 ) is not None:
            tablequery.append(tabletokenize[i])
            for syn in wordnet.synsets(tabletokenize[i], lang='fra'):
                for l in syn.lemmas():
                    synonyms.append(l.name())
            while cpt < 2 and len(synonyms) > c :
                #print(str(len(synonyms))+"  "+str(c)+"\n")
                if synonyms[c]  in tablesyn_fr or (synonyms[c] == tabletokenize[i])  :
                    c = c + 1
                else :
                    cpt = cpt+1


                    c = c + 1
                    # st =  synonyms[c]
                    # r= re.search(r"(\_)" ,st)
                    # print(r)
                    # if r is not None:
                    #     st = st.replace("_", " ")
                    #     synonyms[c] = s
                    tablesyn_fr.append(synonyms[c])
            synonyms.clear()
            z = z + cpt
            i = i + 1
            c = 0
        else:
            i = i + 1


    translations = translator.translate(tablesyn_fr, dest='en', src='fr')
    for translation in translations:
        tablesyn_en.append(translation.text)

    translations = translator.translate(tablesyn_fr, dest='es', src='fr')
    for translation in translations:
        tablesyn_es.append(translation.text)

    translations = translator.translate(tablequery, dest='es', src='fr')
    for translation in translations:
        tablequery_es.append(translation.text)

    translations = translator.translate(tablequery, dest='en', src='fr')
    for translation in translations:
        tablequery_en.append(translation.text)

    for i in tablequery:
        if i not in tablequery_en:
            tablequery_en.append(i)
        if i not in tablequery_es:
            tablequery_es.append(i)
    l={}
    l['requete']=tablequery
    l['trad_syn_anglais']=tablesyn_en
    l['synonymes']=tablesyn_fr
    l['trad_syn_espagnol']=tablesyn_es
    l['trad_requete_anglais']=tablequery_en
    l['trad_requete_espagnol']=tablequery_es
    # l=[]
    # l.append(tablequery)
    # l.append(tablesyn_en)
    # l.append(tablesyn_fr)
    # l.append(tablesyn_es)
    # l.append(tablequery_en)
    # l.append(tablequery_es)

    return l
def trad_es(query):
    print("Translate " + query +" to french and english")
    table_nom_propre = []
    table_nom_propre_fr = []
    table_nom_propre_en = []
    np = 0
    #nlpes = StanfordCoreNLP(r'/home/rania/stanford-corenlp-full-2017-06-09', lang='es')
    nlpes = StanfordCoreNLP(r'/Library/Java/JavaVirtualMachines/jdk-9.0.1.jdk/Contents/Home/lib/stanford-corenlp-full-2017-06-09', lang='es')
    synonyms = []
    i = 0
    z = 1
    c = 0
    cpt = 0

    tablequery = []
    tablequery_fr = []
    tablequery_en = []

    tablesyn_fr = []
    tablesyn_es = []
    tablesyn_en = []

    tabletokenize = []
    tabletagged =[]
    #query = input("rechercher :" )
    def get_sp_synonym(word):
        #prend des synonyms de site web sinonimos.com de le mot donne
        print("method synonyms launched")
        syn_source =  urlopen("https://www.sinonimos.es/espanol/?q=" + str(word))
        syn_soup = BeautifulSoup(syn_source, "html.parser")
        
        synList2=[]
        synonym2=syn_soup.find_all("div", class_="col-xs-6 col-sm-9")
        if len(synonym2) > 0:
            synonym2.pop(0)
            for tag in synonym2:
                #print(tag.name)
                tagType = type(tag)
                if ("Tag" in str(tagType)):
                    for a in tag:
                        tagType1 = type(a)
                        #print(a.name)
                        if (a.name =="a" and len(a.get_text()) > 0):
                            #print(len(a.get_text()))
                            synList2.append(a.get_text()) 

            return synList2   
    #créer une liste de mot , ponctuation ....
    tabletokenize = WordPunctTokenizer().tokenize(query)
    # tagger les entité
    tabletagged = nlpes.pos_tag(query)
    #tabletagged = nltk.pos_tag(tabletokenize)
    for word in tabletagged:
        stringword = str(word)
        r1 = re.search(r"(n)" , stringword)
        r2 = re.search(r"'(v(.))'" , stringword)
        r3 = re.search(r"(np|[A-Z])" , stringword)
        ###rg pour les numérique r3 = re.search(r"(np|rg)" , stringword)
        print(word)
        if (r1 or r2 or r3 ) is not None:
            print("Matching grammar")
            tablequery.append(tabletokenize[i])
            synonymList = get_sp_synonym(tabletokenize[i])
            if synonymList and len(synonymList) > 0:
                for syn in synonymList:
                    synonyms.append(syn)
                while cpt < 2 and len(synonyms) > c :
                    #print(str(len(synonyms))+"  "+str(c)+"\n")
                    if synonyms[c]  in tablesyn_es or (synonyms[c] == tabletokenize[i])  :
                        c = c + 1
                    else :
                        # st =  synonyms[c]
                        # r= re.search(r"(\_)" ,st)
                        # print(r)
                        # if r is not None:
                        #     st = st.replace("_", " ")
                        #     synonyms[c] = st
                        cpt = cpt+1
                        tablesyn_es.append(synonyms[c])
                        c = c + 1
                synonyms.clear()
                z = z + cpt
                i = i + 1
                c = 0

       

        else:
            i = i + 1
            print("not matching grammar")


    translations = translator.translate(tablesyn_es, dest='fr')
    for translation in translations:
        tablesyn_fr.append(translation.text)

    translations = translator.translate(tablesyn_es, dest='en')
    for translation in translations:
        tablesyn_en.append(translation.text)

    translations = translator.translate(tablequery, dest='en')
    for translation in translations:
        tablequery_en.append(translation.text)

    translations = translator.translate(tablequery, dest='fr')
    for translation in translations:
        tablequery_fr.append(translation.text)

    for i in tablequery:
        if i not in tablequery_en:
            tablequery_en.append(i)
        if i not in tablequery_fr:
            tablequery_fr.append(i)
    l={}
    l['requete']=tablequery
    l['trad_syn_anglais']=tablesyn_en
    l['trad_syn_français']=tablesyn_fr
    l['synonymes']=tablesyn_es
    l['trad_requete_français']=tablequery_fr
    l['trad_requete_anglais']=tablequery_en
    # l=[]
    # l.append(tablequery)
    # l.append(tablesyn_en)
    # l.append(tablesyn_fr)
    # l.append(tablesyn_es)
    # l.append(tablequery_fr)
    # l.append(tablequery_en)

    return l

from django.views.generic import TemplateView
from django.http import HttpResponse
from django.shortcuts import render
from django.template import loader, Context
from recherche.forms import RechercheForm
import urllib.request, lxml
import re
import sys
from urllib.request import urlopen
from bs4 import BeautifulSoup
import subprocess

def correspondance_articles(requete,synonyme, lien):
	source =  urlopen(lien).read()
	soup = BeautifulSoup(source, "lxml-xml")
	
	#Get list of dict items
	itemTags = soup.find_all('item')
	item = {}
	itemsList = []
	cpt=0;

	for tag in itemTags:
		#print(tag.name)
		for child in tag.contents:
			if child.name != None:
				if (child.name == "enclosure" or child.name == "thumbnail"):
					item[child.name] = child['url']
				else:
					item[child.name] = child.get_text()
		itemsList.append(item.copy())
	#for line in itemsList:
		#print(line['title'], file=sys.stderr)
	#Populate list of matching articles
	matchedItems = []
	m=[]
	liste_triee=[]
	for item in itemsList:
	#print(item['title'], file=sys.stderr)
		if not any(d['title'] == str(item['title']) for d in matchedItems):
			for mot in requete:
			    s=r"\b"+mot+r"\b"
			    if re.search(s, str(item['title']), re.IGNORECASE):
			    	cpt=cpt+4;
			    if re.search(s, str(item['description']), re.IGNORECASE):
			    	cpt=cpt+2;
			for mot in synonyme:
			    s=r"\b"+mot+r"\b"
			    if re.search(s, str(item['title']), re.IGNORECASE):
			    	cpt=cpt+3;
			    if re.search(s, str(item['description']), re.IGNORECASE):
			    	cpt=cpt+1;
			if (cpt>0):
			    matchedItems.append(item)
			    s=(item,cpt)
			    m.append(s)
			   # print(cpt)
			    cpt=0
	m=sorted(m, key=lambda colonnes: colonnes[1], reverse=True)
	for i in m:
		liste_triee.append(i[0])
	#print(liste_triee)
	## possibilité de réduire la liste : garder les 5 premiers elements par exemple
		    	

	return liste_triee
